#!/bin/bash

set -x

HOST_IP="192.168.10.20"

usage() {
    echo "Usage: ${0} [-h HOST_IP]
" 1>&2
    exit 0
}

while getopts "h:" arg; do
  case "$arg" in
    h)
      HOST_IP="${OPTARG}"
      ;;
    ?)
      usage
      echo "unrecognized argument ${OPTARG}"
      ;;
  esac
done


yum install unzip automake -y
wget https://github.com/HewlettPackard/netperf/archive/refs/heads/master.zip
unzip master.zip
cd netperf-master
\cp /usr/share/automake-1.16/config.guess .
\cp /usr/share/automake-1.16/config.sub .
./autogen.sh
./configure
make CFLAGS=-fcommon
make install
netperf -V

for i in 1 64 128 256 512 1024 1500 2048 4096 9000 16384 32768 65536;do
netperf -t TCP_STREAM -H ${HOST_IP} -l 60 -- -m ${i}
done

for i in 1 64 128 256 512 1024 1500 2048 4096 9000 16384 32768;do
netperf -t UDP_STREAM -H ${HOST_IP} -l 60 -- -m ${i} -R 1
done

netperf -t TCP_RR -H ${HOST_IP}
netperf -t TCP_CRR -H ${HOST_IP}
netperf -t UDP_RR -H ${HOST_IP}
