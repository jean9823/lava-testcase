#!/bin/bash

set -x

COPIES="1"

usage() {
    echo "Usage: ${0} [-c COPYIES]
" 1>&2
    exit 0
}

while getopts "c:" arg; do
  case "$arg" in
    c)
      COPIES="${OPTARG}"
      ;;
    ?)
      usage
      echo "unrecognized argument ${OPTARG}"
      ;;
  esac
done


yum install unzip
wget https://github.com/kdlucas/byte-unixbench/archive/refs/heads/master.zip
unzip master.zip
cd byte-unixbench-master/UnixBench
make
./Run -c ${COPIES}
