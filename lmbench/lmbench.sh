#!/bin/bash

set -x

MULTIPLE_COPIES=""
JOB_PLACEMENT_SELECTION=""
MB=""
SUBSET=""
FASTMEM=""
SLOWFS=""
DISKS=""
REMOTE=""
PROCESSER=""
FSDIR=""
STATUS_OUTPUT_FILE=""
MAIL_RESULTS="no"

usage() {
    echo "Usage: ${0} [-c MULTIPLE_COPIES]
                      [-j JOB_PLACEMENT_SELECTION]
                      [-M MB]
                      [-S SUBSET]
                      [-F FASTMEM]
                      [-s SLOWFS]
                      [-d DISKS]
                      [-r REMOTE]
                      [-p PROCESSER]
                      [-f FSDIR]
                      [-o STATUS_OUTPUT_FILE]
                      [-m MAIL_RESULTS]
" 1>&2
    exit 0
}

while getopts "c:j:M:S:F:s:d:r:p:f:o:m:" arg; do
  case "$arg" in
    c)
      MULTIPLE_COPIES="${OPTARG}"
      ;;
    j)
      JOB_PLACEMENT_SELECTION="${OPTARG}"
      ;;
    M)
      MB="${OPTARG}"
      ;;
    S)
      SUBSET="${OPTARG}"
      ;;
    F)
      FASTMEM="${OPTARG}"
      ;;
    s)
      SLOWFS="${OPTARG}"
      ;;
    d)
      DISKS="${OPTARG}"
      ;;
    r)
      REMOTE="${OPTARG}"
      ;;
    p)
      PROCESSER="${OPTARG}"
      ;;
    f)
      FSDIR="${OPTARG}"
      ;;
    o)
      STATUS_OUTPUT_FILE="${OPTARG}"
      ;;
    m)
      MAIL_RESULTS="${OPTARG}"
      ;;
    ?)
      usage
      echo "unrecognized argument ${OPTARG}"
      ;;
  esac
done

yum -y install libtirpc libtirpc-devel automake

wget https://sourceforge.net/projects/lmbench/files/development/lmbench-3.0-a9/lmbench-3.0-a9.tgz
tar xzvf lmbench-3.0-a9.tgz
cd lmbench-3.0-a9

ln -s /usr/include/tirpc/netconfig.h /usr/include/
cp /usr/include/tirpc/rpc/* /usr/include/rpc/
\cp -rf /usr/share/automake-1.16/config.guess scripts/gnu-os


echo -e "${MULTIPLE_COPIES}\n${JOB_PLACEMENT_SELECTION}\n${MB}\n${SUBSET}\n${FASTMEM}\n${SLOWFS}\n${DISKS}\n${REMOTE}\n${PROCESSER}\n${FSDIR}\n${STATUS_OUTPUT_FILE}\n${MAIL_RESULTS}\n" | make LDFLAGS=-ltirpc results

make see
cat results/summary.out
